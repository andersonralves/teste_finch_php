FROM php:7.3-apache

RUN docker-php-ext-install pdo pdo_mysql

RUN bash -c "a2enmod rewrite && a2enmod headers && service apache2 restart"