<?php

declare(strict_types=1);

namespace App\Traits;

trait WorkflowTrait
{
    public function workflow()
    {
        echo $this->template->render("workflow/listar");
    }

    public function workflow_novo()
    {
        echo $this->template->render("workflow/novo");
    }

    public function workflow_visualizar($data)
    {
        $id = (int) $data["id"];
        echo $this->template->render("workflow/visualizar", ["id" => $id]);
    }
}