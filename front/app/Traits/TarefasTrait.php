<?php

declare(strict_types=1);

namespace App\Traits;

trait TarefasTrait
{

    public function home()
    {
        echo $this->template->render("dashboard");
    }

    public function tarefas()
    {
        echo $this->template->render("tarefas/listar");
    }

    public function tarefas_novo()
    {
        echo $this->template->render("tarefas/novo");
    }

    public function tarefas_editar($data)
    {
        $id =  (int) $data["id"];

        echo $this->template->render("tarefas/editar", ["id" => $id]);
    }
}