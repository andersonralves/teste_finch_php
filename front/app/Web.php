<?php

declare(strict_types=1);

namespace App;

use App\Traits\TarefasTrait;
use App\Traits\WorkflowTrait;

class Web
{
    use TarefasTrait, WorkflowTrait;

    private $template;

    public function __construct()
    {
        $this->template = new \League\Plates\Engine(ROOTPATH.DS.'views');
    }
    
    public function ooops()
    {
        echo $this->template->render("ooops");
    }



    
}