<!doctype html>
<html lang="pt-BR">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= url_site() ?>/assets/bootstrap/css/bootstrap-4.4.1.min.css">
    <link rel="stylesheet" href="<?= url_site() ?>/assets/_css/dashboard.css">

    <title>Finch Soluções - Teste Analista Desenvolvedor PHP</title>

    <script>var URL_API = "<?= url_api(); ?>";</script>
</head>
<body>

    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
            <div class="sidebar-heading">
                <a href="<?= url_site() ?>">
                    <img src="<?= url_site() ?>/assets/img/logo_finch.png" alt="">
                </a>
            </div>

            <div class="list-group list-group-flush">
                <a href="<?= url_site(); ?>/tarefas" class="list-group-item list-group-item-action bg-light">Tarefas</a>
                <a href="<?= url_site(); ?>/workflow" class="list-group-item list-group-item-action bg-light">Fluxo de Trabalho</a>
            </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <button class="btn btn-primary btn-sm" id="menu-toggle" style="display: inline-block !important;">
                <span class="carousel-control-next-icon"></span>
            </button>

            <div class="container-fluid">

               <div class="jumbotron text-center">
                   <h3>Teste Analista Desenvolvedor PHP</h3>
                   <h6 class="text-right mb-0 pb-0">
                       <a href="mailto:andersonricardo.alves@gmail.com" title="Anderson Alves <andersonricardo.alves@gmail.com>" >Anderson Alves</></a>
                   </h6>
               </div>

                <?=$this->section('content')?>

            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <footer>
        # <a href="mailto:andersonricardo.alves@gmail.com" title="Anderson Alves <andersonricardo.alves@gmail.com>">Anderson Alves - 2019</a>
    </footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?= url_site() ?>/assets/jquery/jquery-3.4.1.min.js"></script>
<script src="<?= url_site() ?>/assets/bootstrap/js/bootstrap-4.4.1.bundle.min.js"></script>
<script>

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    function alertErrorResponse(response)
    {
        var res = $.parseJSON(response.responseText);
        alert(res.message);
    }

</script>

<?=$this->section('scripts')?>

</body>
</html>