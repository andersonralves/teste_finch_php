<?php $this->layout('dashboard') ?>

<a href="<?= url_site() ?>/tarefas" class="btn btn-outline-primary">Listar Tarefas</a>

<div class="row">
    <div class="col-sm-6 offset-sm-3">

        <h2 class="text-primary">Nova Tarefa</h2>

        <div class="form-group">
            <label for="titulo">Título</label>
            <input type="text" class="form-control" id="titulo" name="titulo">
        </div>

        <div class="form-group">
            <label for="tipo">Tipo</label>
            <select class="form-control" id="tipo" name="tipo"></select>
        </div>

        <button type="button" id="btnSalvar" class="btn btn-success">Salvar</button>
    </div>
</div>

<?php $this->start("scripts"); ?>

<script>
    function selecionarTiposDeTarefa()
    {
        $.ajax({
            url: URL_API  + "/tasktypes",
            type: "GET",
            contentType: "json",
            success: function (response) {

                if (response.success) {
                    // Tipos de Tarefa
                    $.each(response.data.task_types, function(i, value){
                        var opt = "<option value='" + value.id + "'>" + value.title + "</option>";
                        $('#tipo').append(opt);
                    });

                } else {
                    alert("Não foi possível criar a tarefa");
                }
            },
            error: function (response) {
                alertErrorResponse(response);
            }
        });
    }

    function salvarTarefa()
    {
        $.ajax({
            url: URL_API  + "/tasks/store",
            type: "POST",
            data: {
                title: $("#titulo").val(),
                tasktype_id: $("#tipo").val()
            },
            contentType: "json",
            success: function (response) {

                if (response.success) {
                    alert("Tarefa gravada com sucesso");

                    window.location.href = "<?= url_site(); ?>/tarefas";
                } else {
                    alert(response.message);
                }
            },
            error: function (response) {
                alertErrorResponse(response);
            }

        });
    }


    $(document).ready(function(){
        selecionarTiposDeTarefa();

        // Salvar Tarefa
        $('#btnSalvar').click(function() {
            salvarTarefa();
        });
    });
</script>

<?php $this->end(); ?>
