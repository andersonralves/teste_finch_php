<?php $this->layout('dashboard') ?>

    <a href="<?= url_site() ?>/tarefas" class="btn btn-outline-primary">Listar Tarefas</a>
    <div class="row">
        <div class="col-sm-6 offset-sm-3">

            <input type="hidden" name="id" id="id" value="<?= $id; ?>">

            <h2 class="text-primary">Editar Tarefa</h2>

            <div class="form-group">
                <label for="titulo">Título</label>
                <input type="text" class="form-control" id="titulo" name="titulo">
            </div>

            <div class="form-group">
                <label for="tipo">Tipo</label>
                <select class="form-control" id="tipo" name="tipo"></select>
            </div>

            <button type="button" id="btnSalvar" class="btn btn-success">Salvar</button>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-6 offset-sm-3">

            <h3 class="text-info">Botões da Tarefa</h3>
            <table id="tblBotoes" class="table table-striped">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Tarefa Destino</th>
                    <th>#</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>
    </div>
    <hr>

    <div class="row py-3">
        <div class="col-sm-6 offset-sm-3 bg-secondary">

            <h4>Novo Botão</h4>

            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" name="nome">
            </div>

            <div class="form-group">
                <label for="title">Destino</label>
                <select class="form-control" id="tarefa_destino" name="tarefa_destino"></select>
            </div>

            <button type="button" id="btnAdicionarBotao" class="btn btn-primary my-2">Adicionar</button>
        </div>
    </div>

    <!-- Modal Editar Botão-->
    <div class="modal fade" id="modal-botao-editar" tabindex="-1" role="dialog" aria-labelledby="modal-botao-editar" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar Tarefa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-editar" name="id-editar">

                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" id="nome-editar" name="nome-editar">
                    </div>

                    <div class="form-group">
                        <label for="title">Destino</label>
                        <select class="form-control" id="tarefa_destino-editar" name="tarefa_destino-editar"></select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="btnAtualizarBotao">Atualizar</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->start("scripts"); ?>

    <script>

        var tarefa_id = "<?= $id; ?>";

        function selecionarTarefas()
        {
            $.ajax({
                url: URL_API  + "/tasks",
                type: "GET",
                contentType: "json",
                success: function (response) {
                    if (response.data.tasks) {
                        $.each(response.data.tasks, function(i, value) {
                            var option = "<option value='"+ value.id +"'>" + value.title + "</option>";

                            $("#tarefa_destino").append(option);
                            $("#tarefa_destino-editar").append(option);
                        });
                    }
                }
            });
        }

        function selecionarTarefa()
        {
            $.ajax({
                url: URL_API  + "/tasks/" + tarefa_id,
                type: "GET",
                contentType: "json",
                success: function (response) {
                    var task = response.data.task;
                    $('#titulo').val(task.title)
                    $('#tipo').val(task.tasktype_id);

                    $("#tblBotoes tbody").empty();

                    // Preenchendo os botões da tarefa
                    if (task.buttons != null) {

                        $.each(task.buttons, function(i, value) {
                            var tr = "<tr>";
                            tr += "<td>" + value.name + "</td>";
                            tr += "<td>" + value.next_tasks_id + "</td>";
                            tr += '<td> <button type="button" class="btn btn-sm btn-primary editar-botao" data-toggle="modal"'
                                + ' data-id="' + value.id + '" data-name="' + value.name + '"'
                                + ' data-next-tasks-id="' + value.next_tasks_id + '" data-target="#modal-botao-editar">Editar</button>';
                            tr += '  <button type="button" class="btn btn-sm btn-danger excluir-botao" data-id="' + value.id + '">Excluir</td>';
                            tr += "<tr>";

                            $("#tblBotoes tbody").append(tr);

                            $('.editar-botao').click(function() {
                                var data = $(this).data();

                                $('#id-editar').val(data.id);
                                $('#nome-editar').val(data.name);
                                $('#tarefa_destino-editar').val(data.nextTasksId);
                            });

                            $('.excluir-botao').click(function() {
                                var idBotao = $(this).data('id');
                                excluirBotao(idBotao);
                            });

                        });
                    }
                }
            });
        }

        function selecionarTiposDeTarefa()
        {
            $.ajax({
                url: URL_API  + "/tasktypes",
                type: "GET",
                contentType: "json",
                success: function (response) {

                    if (response.success) {

                        // Tipos de Tarefa
                        $.each(response.data.task_types, function(i, value){
                            var opt = "<option value='" + value.id + "'>" + value.title + "</option>";
                            $('#tipo').append(opt);
                        });

                        selecionarTarefa();
                    }
                }
            });
        }

        function atualizarTarefa()
        {
            $.ajax({
                url: URL_API  + "/tasks/update/" + tarefa_id,
                type: "PUT",
                data: {
                    title: $("#titulo").val(),
                    tasktype_id: $("#tipo").val()
                },
                contentType: "json",
                success: function (response) {

                    if (response.success) {
                        alert("Tarefa gravada com sucesso");
                    } else {
                        alert(response.message);
                    }
                },
                error: function (response) {
                    alertErrorResponse(response);
                }

            });
        }

        function adicionarBotao()
        {
            $.ajax({
                url: URL_API  + "/buttons/store",
                type: "POST",
                data: {
                    name: $("#nome").val(),
                    tasks_id: tarefa_id,
                    next_tasks_id: $("#tarefa_destino").val(),
                },
                contentType: "json",
                success: function (response) {

                    if (response.success) {

                        $('#nome').val('');

                        //alert("Botão criado com sucesso");

                        selecionarTarefa();
                    } else {
                        alert(response.message);
                    }
                },
                error: function (response) {
                    alertErrorResponse(response);
                }

            });
        }

        function atualizarBotao()
        {
            $.ajax({
                url: URL_API  + "/buttons/update/" +  + $('#id-editar').val(),
                type: "PUT",
                data: {
                    name: $('#nome-editar').val(),
                    tasks_id: $('#id').val(),
                    next_tasks_id: $('#tarefa_destino-editar').val()
                },
                contentType: "json",
                success: function (response) {

                    if (response.success) {
                        $('#modal-botao-editar').modal('hide');
                        selecionarTarefa();
                    } else {
                        alert(response.message);
                    }
                },
                error: function (response) {
                    alertErrorResponse(response);
                }

            });
        }

        function excluirBotao($id)
        {
            $.ajax({
                url: URL_API  + "/buttons/destroy/" + $id,
                type: "DELETE",
                contentType: "json",
                success: function (response) {
                    selecionarTarefa();
                    //alert("Botão excluido com sucesso");
                },
                error: function (response) {
                    alertErrorResponse('ss');
                }

            });
        }

        $(document).ready(function() {

            selecionarTarefas();

            selecionarTiposDeTarefa();

            // Salva a tarefa
            $('#btnSalvar').click(function() {
                atualizarTarefa();
            });

            // Adiciona um botão
            $('#btnAdicionarBotao').click(function() {
                adicionarBotao();
            });

            $('#btnAtualizarBotao').click(function() {
                atualizarBotao();
            });

        });
    </script>

<?php $this->end(); ?>