<?php $this->layout('dashboard') ?>

<a href="<?= url_site(); ?>/tarefas/novo" class="btn btn-outline-primary">Nova Tarefa</a>

<div class="row">
    <div class="col-6 offset-3">
        <h2 class="text-primary">Tarefas</h2>
        <table id="tblTarefas" class="table table-striped">
            <thead>
            <tr>
                <th>Código</th>
                <th>Título</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>

    </div>
</div>

<a href="<?= url_site(); ?>/tarefas/novo" class="btn btn-outline-primary">Nova Tarefa</a>

<?php $this->start("scripts"); ?>
<script>

    function buscarTarefas()
    {
        $.ajax({
            url: URL_API  + "/tasks",
            type: "GET",
            contentType: "json",         
            success: function (response) {
                $("#tblTarefas tbody").empty();
                $.each(response.data.tasks, function(i, value) {
                    var tr = "<tr>";
                    tr += "<td>" + value.id + "</td>";
                    tr += "<td>" + value.title + "</td>";

                    tr += "<td>";
                    tr += "<a href='<?= url_site() ?>/tarefas/editar/" + value.id + "' class='btn btn-sm btn-primary' title='Editar Tarefa'>Editar<a>" ;
                    if (value.id != 1) {
                        tr += " <button type='button' id='btnExcluirTarefa' class='btn btn-sm btn-danger excluir-tarefa'" +
                            " data-id='" + value.id + "' title='Excluir'>Excluir</button>";
                    }
                    tr += "</td>";

                    tr += "</tr>";

                    $("#tblTarefas tbody").append(tr);

                    $('.excluir-tarefa').click(function() {
                        var idTarefa = $(this).data('id');
                        excluirTarefa(idTarefa);
                    });
                });

            }
        });
    }

    function excluirTarefa($id)
    {
        $.ajax({
            url: URL_API  + "/tasks/destroy/" + $id,
            type: "DELETE",
            contentType: "json",
            success: function (response) {
                if (response.success) {
                    buscarTarefas();
                } else {
                    alert("Não foi possivel excluir a tarefa");
                }

            },
            error: function (response) {
                alertErrorResponse(response);
            }

        });
    }

    $(document).ready(function() {

        buscarTarefas();
    });
</script>
<?php $this->stop(); ?>
