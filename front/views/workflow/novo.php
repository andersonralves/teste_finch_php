<?php $this->layout('dashboard') ?>

<a href="<?= url_site(); ?>/workflow" class="btn btn-outline-primary">Listar Workflows</a>

<div class="row">

    <div class="col-sm-6 offset-sm-3">

        <h2 class="text-primary">Novo Workflow</h2>

        <div class="form-group">
            <label for="tarefa">Tarefa de Abertura</label>
            <select id="tarefa" name="tarefa" class="form-control"></select>
        </div>

        <div class="form-group">
            <label for="observacoes">Observações</label>
            <textarea id="observacoes" name="observacoes" class="form-control" rows="6" maxlength="500"></textarea>
        </div>

        <button class="btn btn-success" id="btnSalvarWorkflow">Salvar</button>
    </div>

</div>

<?php $this->start("scripts"); ?>
<script>

    function buscarTarefasDeAbertura()
    {
        $.ajax({
            url: URL_API + "/tasks/ofstart",
            type: "GET",
            contentType: "json",
            success: function (response) {

                if (response.success) {
                    // Tipos de Tarefa
                    $.each(response.data.tasks, function (i, value) {
                        var opt = "<option value='" + value.id + "'>" + value.title + "</option>";
                        $('#tarefa').append(opt);
                    });
                }
            }
        });
    }

    function salvarWorkflow()
    {
        $.ajax({
            url: URL_API + "/workflows/store",
            type: "POST",
            data: {
                tasks_id_start: $('#tarefa').val(),
                description: $('#observacoes').val()
            },
            contentType: "json",
            success: function (response) {

                if (response.success) {
                    var workflowId = response.data.workflow.id;

                    alert('Workflow criado com sucesso');

                    window.location.href = "<?= url_site() ?>/workflow/visualizar/" + workflowId;
                } else {
                    alert('Não foi possivel criar o workflow');
                }

            },
            error: function (response) {
                alertErrorResponse(response);
            }
        });
    }

    $(document).ready(function() {

        buscarTarefasDeAbertura();

        $('#btnSalvarWorkflow').on('click', salvarWorkflow);
    });
</script>
<?php $this->stop(); ?>
