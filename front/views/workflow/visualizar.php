<?php $this->layout('dashboard') ?>

<a href="<?= url_site(); ?>/workflow" class="btn btn-outline-primary">Listar Workflows</a>

<div class="row">
    <div class="col-7 offset-2">
        <h2 class="text-primary">Workflow #<?= $id; ?></h2>

        <input type="hidden" id="workflow_id" name="worflow_id" value="<?= $id; ?>">
        <input type="hidden" id="tarefa_id" name="tarefa_id">

        <div class="form-group">
            <label for="tarefa"><strong>Tarefa</strong></label>
            <input type="text" class="form-control" id="tarefa" name="tarefa" readonly>
        </div>

        <div class="form-group">
            <label for="comentarios"><strong>Comentários da Tarefa Anterior</strong></label>
            <textarea id="comentarios" name="comentarios" class="form-control" rows="6" readonly></textarea>
        </div>

        <div class="form-group">
            <label for="observacoes"><strong>Observações</strong></label>
            <textarea id="observacoes" name="observacoes" class="form-control" rows="6" maxlength="500"></textarea>
        </div>

        <div class="form-group">
            <div id="botoes"></div>
        </div>

    </div>

    </div>
</div>


<?php $this->start("scripts"); ?>
<script>

    function nextStepWorkflow(nextTaskId)
    {
        $.ajax({
            url: URL_API  + "/workflows/next/<?= $id ?>",
            type: "PUT",
            data: {
                task_id:  $('#tarefa_id').val(),
                next_task_id: nextTaskId,
                description: $('#observacoes').val()
            },
            contentType: "json",
            success: function (response) {
                if (response.success) {

                    if (response.data.closed == true) {
                    
                        alert('Fluxo encerrado com sucesso');
                        window.location.href = "<?= url_site() ?>/workflow";
                        return;
                    }

                    alert('Fluxo atualizado com sucesso');

                    window.location.href = "<?= url_site() ?>/workflow/visualizar/17";

                } else {
                    alert('Não foi possivel atualizar o fluxo!');
                }

            },
            error: function (response) {
                alertErrorResponse(response);
            }
        });
    }

    function buscarDadosDoWorkflow()
    {
        $.ajax({
            url: URL_API + "/workflows/getData/<?= $id ?>",
            type: "GET",
            contentType: "json",
            success: function (response) {
                if (response.success) {
                    var workflow = response.data.workflow;

                    $('#tarefa_id').val(workflow.tasks_id);
                    $('#tarefa').val(workflow.task_title);
                    $('#comentarios').val(workflow.description)

                    if (response.data.buttons != null) {
                        $.each(response.data.buttons, function (i, value) {
                            var btn = " <button class='btn btn-outline-primary next-step' data-id='" + value.next_tasks_id + "'>"  + value.name + "</button>";

                            $('#botoes').append(btn);      
                        });

                        $('.next-step').click(function() {
                            
                            var data = $(this).data();

                            var id = data.id;

                           nextStepWorkflow(id);
                        });
                        
                    }
                }
            },
            error: function (response) {
                alertErrorResponse(response);
            }
        });
    }

    $(document).ready(function() {
        buscarDadosDoWorkflow();
    });
</script>
<?php $this->stop(); ?>
