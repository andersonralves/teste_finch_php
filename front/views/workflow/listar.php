<?php $this->layout('dashboard') ?>

<a href="<?= url_site(); ?>/workflow/novo" class="btn btn-outline-primary">Novo</a>

<div class="row">
    <div class="col-7 offset-2">
        <h2 class="text-primary">Workflows Pendentes</h2>
        <table id="tblWorkflows" class="table table-striped">
            <thead>
            <tr>
                <th>Código</th>
                <th>Tarefa</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>

    </div>
</div>

<a href="<?= url_site(); ?>/workflow/novo" class="btn btn-outline-primary">Novo</a>

<?php $this->start("scripts"); ?>
<script>

    function buscarWorkflows()
    {
        $.ajax({
            url: URL_API  + "/workflows/pending",
            type: "GET",
            contentType: "json",
            success: function (response) {
                $("#tblWorkflows tbody").empty();

                $.each(response.data.workflows, function(i, value) {
                    var tr = "<tr>";
                    tr += "<td>" + value.id + "</td>";
                    tr += "<td>" + value.tasks_id_start + "</td>";

                    tr += "<td>";
                    tr += "<a href='<?= url_site() ?>/workflow/visualizar/" + value.id + "' class='btn btn-sm btn-success' " +
                        "title='Visualizar'>Visualizar<a>" ;
                    tr += "</td>";

                    tr += "</tr>";

                    $("#tblWorkflows tbody").append(tr);

                });

            }
        });
    }

    $(document).ready(function() {
        buscarWorkflows();
    });
</script>
<?php $this->stop(); ?>
