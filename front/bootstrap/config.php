<?php

/**
 * Arquivo de configurações padrões carregado no autoload do composer
 */

# definindo constants
define('DS', DIRECTORY_SEPARATOR);
define('ROOTPATH', dirname(__DIR__, 1));

define("SITE", [
    "URL" => "http://localhost:8082"
]);

define("API", [
    "URL" => "http://localhost:8081"
]);
