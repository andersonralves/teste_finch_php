<?php

use CoffeeCode\Router\Router;

$router = new Router(SITE["URL"]);

$router->namespace("App");

/**
 * Web
*/
$router->get("/", "Web:home");

/**
 * Web - Tarefas
 */
$router->get("/tarefas", "Web:tarefas");
$router->get("/tarefas/novo", "Web:tarefas_novo");
$router->get("/tarefas/editar/{id}", "Web:tarefas_editar");


/**
 * Web - Workflow
 */
$router->get("/workflow", "Web:workflow");
$router->get("/workflow/novo", "Web:workflow_novo");
$router->get("/workflow/visualizar/{id}", "Web:workflow_visualizar");

$router->get("/ooops", "Web:ooops");

$router->dispatch();

/**
 * Rota não encontrada
 */
if ($router->error()) {
    $router->redirect("/ooops");
  
}



