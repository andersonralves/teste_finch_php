-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.3.15-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para db_finch
CREATE DATABASE IF NOT EXISTS `db_finch` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_finch`;

-- Copiando estrutura para tabela db_finch.buttons
CREATE TABLE IF NOT EXISTS `buttons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `tasks_id` int(11) NOT NULL,
  `next_tasks_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_BUTTONS_TASKS_ID` (`tasks_id`),
  KEY `FK_BUTTONS_NEXT_TASK` (`next_tasks_id`),
  CONSTRAINT `FK_BUTTONS_NEXT_TASK` FOREIGN KEY (`next_tasks_id`) REFERENCES `tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_BUTTONS_TASKS_ID` FOREIGN KEY (`tasks_id`) REFERENCES `tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela db_finch.buttons: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `buttons` DISABLE KEYS */;
/*!40000 ALTER TABLE `buttons` ENABLE KEYS */;

-- Copiando estrutura para tabela db_finch.tasks
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0 - Inativa, 2 - Ativa, 3 - Especial',
  `tasktype_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_TASKS_TASKTYPE_ID` (`tasktype_id`),
  CONSTRAINT `FK_TASKS_TASKTYPE_ID` FOREIGN KEY (`tasktype_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela db_finch.tasks: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` (`id`, `title`, `status`, `tasktype_id`, `created_at`, `updated_at`) VALUES
	(1, 'Encerra o Fluxo', 2, 4, '2020-03-16 00:22:08', '2020-03-16 02:07:43'),
	(2, 'Atender Solicitação', 1, 2, '2020-03-16 00:47:20', '2020-03-16 01:08:57'),
	(3, 'Analisar Retorno do Atendimento', 1, 2, '2020-03-16 00:47:58', '2020-03-16 01:07:45'),
	(4, 'Ciencia da Finalizacao', 1, 3, '2020-03-16 00:51:24', '2020-03-16 00:51:24'),
	(6, 'Solicitar Atendimento Área de TI', 1, 1, '2020-03-16 01:08:53', '2020-03-16 11:21:16');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;

-- Copiando estrutura para tabela db_finch.task_types
CREATE TABLE IF NOT EXISTS `task_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET latin1 NOT NULL,
  `description` varchar(150) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela db_finch.task_types: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `task_types` DISABLE KEYS */;
INSERT INTO `task_types` (`id`, `title`, `description`) VALUES
	(1, 'Abertura', 'Primeira tarefa do fluxo, sempre que iniciar um novo fluxo começ aqui'),
	(2, 'Intermediária', 'Tarefas que não são de abertura nem finalização'),
	(3, 'Finalizadora', 'Sempre que cair nessa tarefa o fluxo será encerrado'),
	(4, 'Encerramento', 'Encerramento do fluxo');
/*!40000 ALTER TABLE `task_types` ENABLE KEYS */;

-- Copiando estrutura para tabela db_finch.workflows
CREATE TABLE IF NOT EXISTS `workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tasks_id_start` int(11) NOT NULL,
  `tasks_id_end` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_WORKFLOW_TASKS_END` (`tasks_id_end`),
  KEY `FK_WORKFLOW_TASKS_START` (`tasks_id_start`),
  CONSTRAINT `FK_WORKFLOW_TASKS_END` FOREIGN KEY (`tasks_id_end`) REFERENCES `tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKFLOW_TASKS_START` FOREIGN KEY (`tasks_id_start`) REFERENCES `tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela db_finch.workflows: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `workflows` DISABLE KEYS */;
/*!40000 ALTER TABLE `workflows` ENABLE KEYS */;

-- Copiando estrutura para tabela db_finch.workflows_tasks
CREATE TABLE IF NOT EXISTS `workflows_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `tasks_id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_WORKFLOW_TASKS_TASKS_ID` (`tasks_id`),
  KEY `FK_WORKFLOW_TASKS_WORKFLOW_ID` (`workflow_id`),
  CONSTRAINT `FK_WORKFLOW_TASKS_TASKS_ID` FOREIGN KEY (`tasks_id`) REFERENCES `tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKFLOW_TASKS_WORKFLOW_ID` FOREIGN KEY (`workflow_id`) REFERENCES `workflows` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela db_finch.workflows_tasks: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `workflows_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `workflows_tasks` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
