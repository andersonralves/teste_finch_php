<?php

date_default_timezone_set('America/Sao_Paulo');

require __DIR__ . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

require __DIR__ . DS . "bootstrap" . DS . "app.php";