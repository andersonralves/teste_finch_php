<?php

declare(strict_types=1);

namespace App\Facades;

use App\Models\WorkflowTaskModel;
use CoffeeCode\DataLayer\Connect;

/**
 * Class Workflow
 * @package App\Facades
 */
class Workflow
{

    /**
     * @param int $workflowId
     * @param int $taskId
     * @param string $description
     * @return WorkflowTaskModel
     */
    public static function updateWorkFlowTasks(int $workflowId, int $taskId, string $description): WorkflowTaskModel
    {
        try {
            $workflowTask = (new WorkflowTaskModel());
            $workflowTask->workflow_id = $workflowId;
            $workflowTask->tasks_id = $taskId;
            $workflowTask->description = $description;
            $workflowTaskCreated = $workflowTask->save();

            if (!$workflowTaskCreated) {
                $log = logger();
                $log->warning($workflowTask->fail());
                responseJson(false, "Não foi possivel atualizar o fluxo", [], 400);
            }

        } catch (\Error $e) {
            (logger())->error($e->getMessage());
        } catch (\Exception $e) {
            (logger())->warning($e->getMessage());
        }

        return $workflowTask;
    }

    public static function getDataWorkflow(int $id)
    {
        try {
            $conn = Connect::getInstance();

            $sqlWorkflow = '';
            $sqlWorkflow .= 'SELECT wt.id, workflow_id, t.id as tasks_id, t.title as task_title, description';
            $sqlWorkflow .= ' FROM workflows_tasks wt join tasks t on t.id = wt.tasks_id';
            $sqlWorkflow .= " WHERE wt.id = ( SELECT MAX(id) FROM workflows_tasks WHERE workflow_id = {$id})";

            $workflow = $conn->query($sqlWorkflow)->fetch(\PDO::FETCH_OBJ);

            $sqlButtonsTaskWorkflow = '';
            $sqlButtonsTaskWorkflow .= 'SELECT buttons.name, buttons.next_tasks_id';
            $sqlButtonsTaskWorkflow .= ' FROM workflows_tasks wt JOIN buttons ON wt.tasks_id = buttons.tasks_id';
            $sqlButtonsTaskWorkflow .= " WHERE wt.id = ( SELECT MAX(id) FROM workflows_tasks WHERE workflow_id = {$id})";

            $buttons = $conn->query($sqlButtonsTaskWorkflow)->fetchAll(\PDO::FETCH_OBJ);

            $result = [
                'workflow' => $workflow,
                'buttons' => empty($buttons)? null : $buttons
            ];

        } catch (\Error $e) {
            (logger())->error($e->getMessage());
        } catch (\Exception $e) {
            (logger())->warning($e->getMessage());
        }

        responseJson(true, "OK", $result, 200);
    }
}