<?php

declare(strict_types=1);

namespace App;

/**
 * Class Controller
 * @package App
 */
class Controller
{
    /**
     * @var \Monolog\Logger
     */
    protected $log;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->log = logger();
    }
}