<?php

declare(strict_types=1);

namespace App;

use CoffeeCode\DataLayer\DataLayer;

/**
 * Class Model
 * @package App
 */
class Model extends DataLayer
{
    /**
     * Model constructor.
     * @param string $table
     */
    public function __construct(string $table)
    {
        parent::__construct($table, []);
    }

    /**
     * @param array $objects
     * @return array
     */
    public static function getData(array $objects)
    {
        $data = [];

        foreach($objects as $object) {
            $data[] = $object->data();
        }

        return $data;
    }
}