<?php

declare(strict_types=1);

namespace App\Models;

use App\Model;

class ButtonModel extends Model
{
    public function __construct()
    {
        parent::__construct("buttons");
    }
}