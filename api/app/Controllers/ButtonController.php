<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Controller;
use App\Facades\Task;
use App\Models\ButtonModel;

/**
 * Class ButtonController
 * @package App\Controllers
 */
class ButtonController extends Controller
{
    public function store()
    {
        try {

            parse_str(file_get_contents("php://input"), $params);
            extract($params);

            $button = new ButtonModel();
            $button->name = filter_var($name, FILTER_SANITIZE_STRING);
            $button->tasks_id = filter_var($tasks_id, FILTER_VALIDATE_INT);
            $button->next_tasks_id = filter_var($next_tasks_id, FILTER_VALIDATE_INT);

            $buttonCreated = $button->save();

            if (!$buttonCreated) {
                $this->log->warning($button->fail());
                responseJson(false, "Não foi possível criar o botão", [], 400);
            }

            $result = $button->data();

            responseJson(true, "Botão criado com sucesso", ["button" => $result], 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->warning($e->getMessage());
            responseJson404();
        }
    }

    public function update($data)
    {
        try {

            parse_str(file_get_contents("php://input"), $params);
            extract($params);

            $id = (int) $data["id"];

            $button = (new ButtonModel())->findById($id);
            if (is_null($button)) {
                responseJson404();
            }

            $button->name = filter_var($name, FILTER_SANITIZE_STRING);
            $button->tasks_id = filter_var($tasks_id, FILTER_VALIDATE_INT);
            $button->next_tasks_id = filter_var($next_tasks_id, FILTER_VALIDATE_INT);

            $buttonUpdated = $button->save();

            if (!$buttonUpdated) {
                $this->log->warning($button->fail());
                responseJson(false, "Não foi possível atualizar o botão", [], 400);
            }

            $result = $button->data();

            responseJson(true, "Botão atualizado com sucesso", ["button" => $result], 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->warning($e->getMessage());
            responseJson404();
        }
    }

    public function destroy($data)
    {
        try {

            $id = (int) $data["id"];

            $button = (new ButtonModel())->findById($id);

            if (is_null($button)) {
                throw new \Exception( __METHOD__ . " - Botão não existe");
            }

            if (!$button->destroy()) {
                responseJson(false, "Não foi possível excluir o botão", [], 400);
            }

            responseJson(true, "Botão excluida com sucesso", [], 204);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->warning($e->getMessage());
            responseJson404();
        }
    }

}