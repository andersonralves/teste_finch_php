<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Controller;
use App\Model;
use App\Models\ButtonModel;
use App\Models\TaskModel;
use App\Models\WorkflowTaskModel;


/**
 * Class TaskController
 * @package App\Controllers
 */
class TaskController extends Controller
{

    public function index()
    {
        try {

            $tasks = (new TaskModel())->find()->fetch(true);

            $result = Model::getData($tasks);

            responseJson(true, "OK", ["tasks" => $result], 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
            responseJson404();
        }
    }

    public function show($data)
    {
        $id = (int) $data["id"];

        try {
            $task = (new TaskModel())->findById($id);

            $taskData = $task->data();

            $buttons = (new ButtonModel())->find("tasks_id = :id", "id={$id}")->fetch(true);            
            $buttonsData = !is_null($buttons)? Model::getData($buttons) : null;
            
            $result["task"] = $taskData;

            $result["task"]->buttons = $buttonsData;

            responseJson(true, "OK", $result, 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
            responseJson404();
        }
    }

    public function store()
    {
        try {

            parse_str(file_get_contents("php://input"), $params);
            extract($params);

            $task = new TaskModel();
            $task->title = filter_var($title, FILTER_SANITIZE_STRING);
            $task->tasktype_id = filter_var($tasktype_id, FILTER_VALIDATE_INT);

            $taskCreated = $task->save();

            if (!$taskCreated) {
                $this->log->warning($task->fail());
                responseJson(false, "Não foi possível criar a tarefa", [], 400);
            }

            $result = $task->data();

            responseJson(true, "Tarefa criada com sucesso", ["task" => $result], 201);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
            responseJson404();
        }

    }

    public function update($data)
    {
        try {           

            parse_str(file_get_contents("php://input"), $params);
            extract($params);

            $id = (int) $data["id"];

            // Não pode editar a tarefa do tipo encerramento
            if ($id == 1) {
                responseJson(false, "Não é permitido editar esta tarefa", [], 400);
            }

            $task = (new TaskModel())->findById($id);
            if (is_null($task)) {
                $this->log->warning($task->fail());
                responseJson404();;
            }

            $task->title = filter_var($title, FILTER_SANITIZE_STRING);
            $task->tasktype_id = filter_var($tasktype_id, FILTER_VALIDATE_INT);

            $taskUpdated = $task->save();


            if (!$taskUpdated) {
                $this->log->warning($task->fail());
                responseJson(false, "Não foi possível criar a tarefa", [], 400);
            }

            $result = $task->data();

            responseJson(true, "Tarefa criada com sucesso", ["task" => $result], 201);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
            responseJson404();
        }

    }

    public function destroy($data)
    {
        try {

            $id = (int) $data["id"];

            $task = (new TaskModel())->findById($id);

            if (is_null($task)) {
                throw new \Exception( __METHOD__ . " - Tarefa não existe");
            }

            if (!$task->destroy()) {
                responseJson(false, "Não foi possível excluir a tarefa", [], 400);
            }

            responseJson(true, "Tarefa excluida com sucesso", [], 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->warning($e->getMessage());
            responseJson404();
        }
    }

    public function pending()
    {
        try {

            $tasks = (new TaskModel())->find("status = :status", "status=1")->fetch(true);

            $result = Model::getData($tasks);

            responseJson(true, "OK", ["tasks" => $result], 200);
        } catch (\Error $e) {
            $this->log->error($e->getMessage());
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
        }

    }

    public function ofStart()
    {
        try {

            $tasks = (new TaskModel())->find("tasktype_id = :type", "type=1")->fetch(true);

            $result = Model::getData($tasks);

            responseJson(true, "OK", ["tasks" => $result], 200);
        } catch (\Error $e) {
            $this->log->error($e->getMessage());
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
        }

    }

    public function pendingbutnotofstart()
    {
        try {

            $tasks = (new TaskModel())->find("status = 1 and tasktype_id in (2,3)")->fetch(true);
            
            $result = Model::getData($tasks);

            responseJson(true, "OK", ["tasks" => $result], 200);
        } catch (\Error $e) {
            $this->log->error($e->getMessage());
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
        }
    }

    public function currentworkflow($data)
    {
        try {

            $workflow_id = (int) $data["workflow_id"];
            $workflowsTask = (new WorkflowTaskModel())->find("workflow_id = {$workflow_id}")->fetch(true);
            $flow = $workflowsTask[0]->data();

            $taskId = (int)$flow->tasks_id;

            $task = (new TaskModel)->findById($taskId);

            $buttons = (new ButtonModel())->find("tasks_id = :id", "id={$taskId}")->fetch(true);
            $buttonsData = !is_null($buttons)? Model::getData($buttons) : null;

            $result = [
                "task" => $task->data(),
                "description" => $flow->description
            ];

            $result["task"]->buttons = $buttonsData;

            responseJson(true, "OK", $result, 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
            responseJson404();
        }
    }

}