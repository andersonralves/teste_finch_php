<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Controller;
use App\Facades\Workflow;
use App\Model;
use App\Models\ButtonModel;
use App\Models\TaskModel;
use App\Models\WorkflowModel;

/**
 * Class WorkflowController
 * @package App\Controllers
 */
class WorkflowController extends Controller
{
    /**
     *
     */
    public function index(): void
    {
        try {

            $workflows = (new WorkflowModel())->find()->fetch(true);

            $result = Model::getData($workflows);

            responseJson(true, "OK", ["workflows" => $result], 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
            responseJson404();
        }
    }

    /**
     *
     */
    public function store()
    {
        try {

            parse_str(file_get_contents("php://input"), $params);
            extract($params);

            $taskId = filter_var($tasks_id_start, FILTER_VALIDATE_INT);
            $description = filter_var($description, FILTER_SANITIZE_STRING);

            $task = (new TaskModel())->findById($taskId);

            if (!is_null($task) && $task->tasktype_id != 1) {
                responseJson(false, "Escolha uma Tarefa de Abertura para iniciar o fluxo", [], 400);
            }

            $workflow = new WorkflowModel();
            $workflow->tasks_id_start = $taskId;
            $workflowCreated = $workflow->save();

            if(!$workflowCreated) {
                $this->log->warning($workflow->fail());
                responseJson(true, "Não foi possivel iniciar o workflow", [], 400);
            }

            // Atualizando fluxo
            Workflow::updateWorkFlowTasks((int) $workflow->id, (int) $workflow->tasks_id_start, $description);

            $result = $workflow->data();

            responseJson(true, "Workflow iniciado com sucesso", ["workflow" => $result], 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
            responseJson404();
        }

    }

    /**
     *
     */
    public function pending()
    {
        try {

            $workflows = (new WorkflowModel())->find("tasks_id_end is null")->fetch(true);

            $result = Model::getData($workflows);

            responseJson(true, "OK", ["workflows" => $result], 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->info($e->getMessage());
            responseJson404();
        }
    }

    /**
     * @param $data
     */
    public function next($data)
    {
        try {
            
            parse_str(file_get_contents("php://input"), $params);
            extract($params);

            $workflowId = (int) $data["id"];
            $taskId = (int) filter_var($task_id, FILTER_VALIDATE_INT);
            $nextTaskId = (int) filter_var($next_task_id, FILTER_VALIDATE_INT);
            $description = filter_var($description, FILTER_SANITIZE_STRING);

            // Tarefa atual
            $task = (new TaskModel())->findById($taskId);

            // Próxima tarefa
            $nextTask = (new TaskModel())->findById($nextTaskId);

            // Se a próxima tarefa for de de encerramento fecha o workflow
            if ($nextTask->tasktype_id == 4) {

                $workflow = (new WorkflowModel())->findById($workflowId);
                $workflow->tasks_id_end = $task->id;
                $workflow->save();

                // Encerrando fluxo
                Workflow::updateWorkFlowTasks($workflowId, $nextTaskId, $description);

                responseJson(true, "Fluxo encerrado com sucesso", ["closed" => true], 200);
            }

            // Atualizando fluxo
            $workflowTask = Workflow::updateWorkFlowTasks($workflowId, $nextTaskId, $description);

            // Botões da próxima tarefa
            $buttons = (new ButtonModel())->find("tasks_id = :id", "id={$nextTaskId}")->fetch(true);
            $buttonsData = !is_null($buttons)? Model::getData($buttons) : null;

            $result = [
                "task" => $task->data(),
                "description" => $description
            ];

            $result["task"]->buttons = $buttonsData;

            responseJson(true, "Fluxo atualizado com sucesso", $result, 200);

        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();

        } catch(\Exception $e) {
            $this->log->warning($e->getMessage());
            responseJson404();
        }

    }

    public function getData($data)
    {
        $id = (int) $data['id'];

        Workflow::getDataWorkflow($id);
    }

}