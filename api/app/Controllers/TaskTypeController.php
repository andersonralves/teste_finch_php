<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Controller;
use App\Model;
use App\Models\TaskTypeModel;

/**
 * Class TaskTypeController
 * @package App\Controllers
 */
class TaskTypeController extends Controller
{
    /**
     *
     */
    public function index()
    {
        try {
            $taskTypes = (new TaskTypeModel())->find()->fetch(true);

            $result = Model::getData($taskTypes);

            responseJson(true, "OK", ["task_types" => $result], 200);
        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->warning($e->getMessage());
            responseJson404();
        }
    }

    /**
     * @param $data
     */
    public function show($data)
    {
        try {
            $id = (int) $data["id"];

            $taskType = (new TaskTypesModel())->findById($id);

            $result = $taskType->data();

            responseJson(true, "OK", ["task_types" => $result], 200);
        } catch (\Error $e) {
            $this->log->error($e->getMessage());
            responseJson404();
        } catch (\Exception $e) {
            $this->log->warning($e->getMessage());
            responseJson404();
        }

    }
}