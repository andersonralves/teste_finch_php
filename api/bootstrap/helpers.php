<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * @param bool $success
 * @param string $message
 * @param array $data
 * @param int $statusCode
 */
function responseJson(bool $success, string $message, array $data, int $statusCode = 200)
{

    http_response_code($statusCode);
    header("Content-Type: application/json");

    $json = [
        "status_code" => $statusCode,
        "success" => $success,
        "message" => $message,
        "data" => $data
    ];

    echo json_encode($json, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
    die;
}

function responseJson404(string $message = "", array $data = [])
{
    http_response_code(404);
    header("Content-Type: application/json");

    $json = [
        "status_code" => 404,
        "success" => false,
        "message" => $message,
        "data" => $data
    ];

    echo json_encode($json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    die;
}

function logger()
{
    $logger = new Logger("local");

    $logger->pushHandler(new StreamHandler(BASEPATH.DS.'logs'.DS.'app.log'), Logger::DEBUG);

    return $logger;

}
