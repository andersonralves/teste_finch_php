<?php

define("DS", DIRECTORY_SEPARATOR);

define("BASEPATH", dirname(__DIR__, 1));

define("SITE", [    
    "URL" => "http://localhost:8081"
]);

define("DATABASE", [
    "mysql" => [
        "driver" => "mysql",
        "host" => "mysql_finch",
        "port" => "3306",
        "dbname" => "db_finch",
        "username" => "user",
        "passwd" => "123456",
        "options" => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
            PDO::ATTR_CASE => PDO::CASE_NATURAL
        ]
    ]
]);

$dbAtual = DATABASE["mysql"];
define("DATA_LAYER_CONFIG", $dbAtual);