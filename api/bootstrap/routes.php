<?php

use CoffeeCode\Router\Router;

$router = new Router(SITE["URL"]);

$router->namespace("App\Controllers");

/**
 * Tarefas
*/
$router->group("tasks");
$router->get("/", "TaskController:index");
$router->get("/{id}", "TaskController:show");
$router->post("/store", "TaskController:store");
$router->put("/update/{id}", "TaskController:update");
$router->delete("/destroy/{id}", "TaskController:destroy");
$router->get("/currentworkflow/{workflow_id}", "TaskController:currentworkflow");
$router->get("/pending", "TaskController:pending");
$router->get("/ofstart", "TaskController:ofstart");
$router->get("/pendingbutnotofstart", "TaskController:pendingbutnotofstart");


/**
 * Tipos de Tarefas
 */
$router->group("tasktypes");
$router->get("/", "TaskTypeController:index");
$router->get("/{id}", "TaskTypeController:show");

/**
 * Botões
 */
$router->group("buttons");
$router->post("/store", "ButtonController:store");
$router->put("/update/{id}", "ButtonController:update");
$router->delete("/destroy/{id}", "ButtonController:destroy");

/**
 * Workflow
 */
$router->group("workflows");
$router->get("/", "WorkflowController:index");
$router->post("/store", "WorkflowController:store");
$router->get("/pending", "WorkflowController:pending");
$router->put("/next/{id}", "WorkflowController:next");
$router->get("/getData/{id}", "WorkflowController:getData");

$router->dispatch();

/**
 * Rota não encontrada
 */
if ($router->error()) {
   responseJson404("Recurso não encontrada");
}



